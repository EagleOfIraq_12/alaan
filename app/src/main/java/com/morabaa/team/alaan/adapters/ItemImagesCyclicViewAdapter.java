package com.morabaa.team.alaan.adapters;

/**
 * Created by eagle on 1/22/2018.
 */

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import org.malcdevelop.cyclicview.CyclicAdapter;


public class ItemImagesCyclicViewAdapter extends CyclicAdapter {
      
      private Context context;
      private String[] mResources = {
            "https://dummyimage.com/300x200/0d5ebd/f917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/1d5ebd/e917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/2d5ebd/d917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/3d5ebd/c917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/4d5ebd/b917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/5d5ebd/a917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/6d5ebd/9917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/7d5ebd/8917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/8d5ebd/7917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/9d5ebd/6917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/ad5ebd/5917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/bd5ebd/4917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/cd5ebd/3917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/dd5ebd/2917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/ed5ebd/1917e0.png&text=AlAan",
            "https://dummyimage.com/300x200/fd5ebd/0917e0.png&text=AlAan",
      };
      
      public ItemImagesCyclicViewAdapter(Context context) {
            this.context = context;
      }
      
      @Override
      public int getItemsCount() {
            return mResources.length;
      }
      
      @Override
      public View createView(int position) {
            ImageView imageView = new ImageView(context);
            Picasso.with(context)
                  .load(mResources[position])
                  .resize(200, 200)
                  .into(imageView);
            return imageView;
      }
      
      @Override
      public void removeView(int position, View view) {
            // Do nothing
      }
}