package com.morabaa.team.alaan.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import com.morabaa.team.alaan.R;
import com.morabaa.team.alaan.adapters.CommentsRecyclerViewAdapter;
import com.morabaa.team.alaan.adapters.ItemImagesCyclicViewAdapter;
import com.morabaa.team.alaan.adapters.ItemImagesPagerAdapter;
import com.morabaa.team.alaan.model.Comment;
import java.util.ArrayList;
import java.util.List;
import org.malcdevelop.cyclicview.CyclicView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemInfoFragment extends Fragment {
      
      float ratingItem;
      String itemName;
      String itemDescription;
      float itemPrice;
      
      public ItemInfoFragment() {
            // Required empty public constructor
      }
      
      
      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
            ratingItem = getArguments().getFloat("ratingItem");
            itemName = getArguments().getString("itemName");
            itemDescription = getArguments().getString("itemDescription");
            itemPrice = getArguments().getFloat("itemPrice");
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_item_info, container, false);
      }
      
      @Override
      public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            
            ViewPager imagesPager = view.findViewById(R.id.imagesPager);
            CyclicView imagesCyclicView = view.findViewById(R.id.imagesCyclicView);
            imagesPager.setPageTransformer(true, new ZoomOutPageTransformer());
            RatingBar ratingItemRating = view.findViewById(R.id.ratingItemRating);
            TextView txtItemDescription = view.findViewById(R.id.txtItemDescription);
            TextView txtItemName = view.findViewById(R.id.txtItemName);
            TextView txtItemPrice = view.findViewById(R.id.txtItemPrice);
            
            imagesPager.setAdapter(new ItemImagesPagerAdapter(getContext()));
            imagesCyclicView.setAdapter(new ItemImagesCyclicViewAdapter(getContext()));
            
            ratingItemRating.setRating(ratingItem);
            txtItemDescription.setText(itemDescription);
            txtItemName.setText(itemName);
            txtItemPrice.setText(itemPrice + "");
            
            RecyclerView commentsRecyclerView =
                  view.findViewById(R.id.commentsRecyclerView);
            
            final TextInputEditText txtAddComment =
                  view.findViewById(R.id.txtAddComment);
            txtAddComment.addTextChangedListener(new TextWatcher() {
                  
                  public void afterTextChanged(Editable s) {
                        int c = (s.toString().split("\n").length) + 1;
                        txtAddComment.setLines(c);
                  }
                  
                  public void beforeTextChanged(CharSequence s, int start,
                        int count, int after) {
                        
                  }
                  
                  public void onTextChanged(CharSequence s, int start,
                        int before, int count) {
                  }
            });
            commentsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            List<Comment> comments = new ArrayList<>();
            for (int r = 0; r < 15; r++) {
                  final int finalR = r;
                  comments.add(
                        new Comment() {{
                              setDate("2018/5/17");
                              setRate(3.2f);
                              setText("fddfgfdhdf fdhfdhetewt ewt ewte y ret3w gte a gtre a");
                              setUserId(finalR);
                              
                        }}
                  );
            }
            commentsRecyclerView.setAdapter(
                  new CommentsRecyclerViewAdapter(getContext(), comments)
            );
      }
      
      
}
