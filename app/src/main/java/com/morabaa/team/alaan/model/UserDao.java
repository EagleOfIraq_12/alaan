package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by thulfkcar on 1/31/2018.
 */

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertUser(User... users);

    @Insert
    public void InsertBothUsers(User user1, User user2); //etc....

    @Delete
    public void deleteUser(User... user);


    @Query("SELECT * FROM User")
    public List<User> USERS();

    @Update
    public void UpdateUser(User... users);


}