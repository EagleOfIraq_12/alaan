package com.morabaa.team.alaan.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.morabaa.team.alaan.R;
import com.morabaa.team.alaan.adapters.ItemsRecyclerViewAdapter;
import com.morabaa.team.alaan.model.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {
    RecyclerView itemsSearchRecyclerView;
    SearchView txtSearch;
    SearchView mainTxtSearch;
    Button btnFilter;
    RadioGroup radiosGroupFilterContainer;
    private List<Item> items = new ArrayList<>();
    private ItemsRecyclerViewAdapter itemsAdapter;
    private View.OnClickListener radioOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            List<Item> currentItems = new ArrayList<>();

            currentItems = itemsAdapter.getItems();
            switch (view.getId()) {
                case (R.id.radioSortByNameAC): {
                    currentItems = Item.sort(currentItems, Item.enumItemProperty.name);
                    break;
                }
                case (R.id.radioSortByNameDC): {
                    currentItems = Item.sort(currentItems, Item.enumItemProperty.name);
                    Collections.reverse(currentItems);
                    break;
                }
                case (R.id.radioSortByPriceAC): {
                    currentItems = Item.sort(currentItems, Item.enumItemProperty.price);
                    break;
                }
                case (R.id.radioSortByPriceDC): {
                    currentItems = Item.sort(currentItems, Item.enumItemProperty.price);
                    Collections.reverse(currentItems);
                    break;
                }
            }
            itemsAdapter.setItems(currentItems);
            itemsSearchRecyclerView.getAdapter().notifyDataSetChanged();
            ((RadioGroup) view.getParent()).setVisibility(View.GONE);
        }
    };

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        txtSearch = view.findViewById(R.id.txtSearch);
        itemsSearchRecyclerView = view.findViewById(R.id.itemsSearchRecyclerView);
        radiosGroupFilterContainer = view.findViewById(R.id.radiosGroupFilterContainer);
        btnFilter = view.findViewById(R.id.btnFilter);

        mainTxtSearch = getActivity().findViewById(R.id.txtSearch);
        txtSearch.setQuery(
                mainTxtSearch.getQuery()
                , false);

        mainTxtSearch.setQuery(
                "", true);
        mainTxtSearch.setVisibility(View.GONE);
        txtSearch.setIconified(true);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radiosGroupFilterContainer.setVisibility(
                        radiosGroupFilterContainer.getVisibility() == View.VISIBLE ?
                                View.GONE : View.VISIBLE
                );
            }
        });

        for (int i = 0; i < radiosGroupFilterContainer.getChildCount(); i++) {
            radiosGroupFilterContainer.getChildAt(i).setOnClickListener(radioOnClickListener);
        }

        items = new ArrayList<>();
        for (int i = 0; i < 150; i++) {
            final int finalI = i;
            items.add(new Item() {
                {
                    setName("منتج " + finalI);
                    setDescription("alaa balaa بلا بلا بلااااا");
                    setPrice(finalI * 12 + finalI % 3);
                    setCategory("cat " + finalI);
                    setImageUrl("https://www.jawdatsaid.net/images/8/86/Clock_derp.png");
                }
            });
        }

        itemsAdapter = new ItemsRecyclerViewAdapter(getContext(), items,
                R.layout.shearched_item_layout,
                getActivity().getSupportFragmentManager().beginTransaction(),
                getActivity());


        itemsSearchRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        itemsSearchRecyclerView.setAdapter(itemsAdapter);

        txtSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //       if (txtSearch.isExpanded() && TextUtils.isEmpty(newText)) {
                callSearch(newText);
                //      }
                return true;
            }

            public void callSearch(String query) {
                List<Item> newItems = new ArrayList<>();
                List<Item> currentItems = new ArrayList<>();

                currentItems = items;

                for (Item item : currentItems) {
                    if (item.getName().contains(query.trim())) {
                        newItems.add(item);
                    }
                }

                itemsAdapter.setItems(newItems);

                itemsSearchRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                itemsSearchRecyclerView.setAdapter(itemsAdapter);
            }

        });

    }
}
