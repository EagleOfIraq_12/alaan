package com.morabaa.team.alaan.adapters;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import com.morabaa.team.alaan.R;
import com.morabaa.team.alaan.model.Comment;
import java.util.List;

/**
 * Created by eagle on 1/22/2018.
 */

public class CommentsRecyclerViewAdapter extends RecyclerView.Adapter<CommentsViewHolder> {
      
      FragmentTransaction fragmentTransaction;
      private List<Comment> comments;
      private Context context;
      
      public CommentsRecyclerViewAdapter(Context context, List<Comment> comments) {
            this.comments = comments;
            this.context = context;
      }
      
      @Override
      public CommentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                  R.layout.comment_layout, parent, false);
            return new CommentsViewHolder(view);
      }
      
      @Override
      public void onBindViewHolder(CommentsViewHolder holder, int position) {
           holder.txtUserComment.setText(
                 comments.get(position).getText()
           );
            holder.txtUserName.setText(
                 comments.get(position).getUserId()+""
           );
            holder.txtUserCommentDate.setText(
                 comments.get(position).getDate()
           );
            holder.ratingUserRating.setRating(
                  comments.get(position).getRate()
            );
      }
      
      @Override
      public int getItemCount() {
            return comments.size();
      }
}

class CommentsViewHolder extends RecyclerView.ViewHolder {
      
      CardView view;
      TextView txtUserName;
      RatingBar ratingUserRating;
      TextView txtUserComment;
      TextView txtUserCommentDate;
      
      public CommentsViewHolder(View itemView) {
            super(itemView);
            view = (CardView) itemView;
            txtUserName = itemView.findViewById(R.id.txtUserName);
            ratingUserRating = itemView.findViewById(R.id.ratingUserRating);
            txtUserComment = itemView.findViewById(R.id.txtUserComment);
            txtUserCommentDate = itemView.findViewById(R.id.txtUserCommentDate);
            
            
      }
}
