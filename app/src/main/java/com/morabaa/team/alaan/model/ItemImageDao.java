package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

/**
 * Created by thulfkcar on 2/1/2018.
 */

@Dao
interface ItemImageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertItemImage(ItemImage... itemImages);

}
