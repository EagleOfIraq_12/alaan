package com.morabaa.team.alaan.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.morabaa.team.alaan.R;
import com.morabaa.team.alaan.fragments.ItemInfoFragment;
import com.morabaa.team.alaan.model.Item;
import com.morabaa.team.alaan.utils.SV;
import com.morabaa.team.alaan.utils.SV.ScreenDimensions;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by eagle on 1/22/2018.
 */

public class ItemsRecyclerViewAdapter extends RecyclerView.Adapter<ItemsViewHolder> {

    private FragmentTransaction fragmentTransaction;
    private List<Item> items;
    private Context context;
    private Activity activity;
    private int layout;

    public ItemsRecyclerViewAdapter(Context context, List<Item> items,
                                    int layout, FragmentTransaction fragmentTransaction,
                                    Activity activity) {
        this.items = items;
        this.context = context;
        this.fragmentTransaction = fragmentTransaction;
        this.activity = activity;
        this.layout = layout;
    }

    @Override
    public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                layout, parent, false);
        return new ItemsViewHolder(view);
    }

    @SuppressLint({"StaticFieldLeak", "SetTextI18n"})
    @Override
    public void onBindViewHolder(final ItemsViewHolder holder, int position) {

        int w = (int) (ScreenDimensions.getWidth(activity) / 2.12);
        if (layout == R.layout.shearched_item_layout) {
            w = (int) (ScreenDimensions.getWidth(activity) / 4.12);

        }
        holder.imgItemImage.getLayoutParams().width = w;
        holder.imgItemImage.getLayoutParams().height = w;

        holder.view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.findViewById(R.id.txtSearch).setVisibility(View.GONE);
                Bundle bundle = new Bundle();
                bundle.putFloat("ratingItem", 2.7f);
                bundle.putString("itemName", "item X ");
                bundle.putString("itemDescription",
                        "txtItemPricetxtItemPrice txtItemPricetxtItemPrice \ntxtItemPrice txtItemPrice");
                bundle.putString("itemPrice", "520,750");
                Fragment commentsFragment = new ItemInfoFragment();
                commentsFragment.setArguments(bundle);

                SV.openfrg(context, commentsFragment, "commentsFragment");
//                        fragmentTransaction.add(R.id.mainFrame, commentsFragment)
//                              .addToBackStack(null).commit();
//                        fragmentTransaction = null;

            }
        });
        holder.txtItemName.setText(items.get(position).getName());
        holder.txtItemDescription.setText(items.get(position).getDescription());
        holder.txtItemPrice.setText(items.get(position).getPrice() + " دينار عراقي");

        Picasso.with(context)
                .load(items.get(position).getImageUrl())
                .into(holder.imgItemImage);

//            new DownloadImage() {
//                  @Override
//                  public void onDownload(Bitmap result) {
//                        holder.imgItemImage.setImageBitmap(result);
//                  }
//            }.execute(items.get(position).getImageUrl());


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
//            notifyAll();
        notifyDataSetChanged();
    }
}

class ItemsViewHolder extends RecyclerView.ViewHolder {

    CardView view;
    ImageView imgItemImage;
    TextView txtItemName;
    TextView txtItemDescription;
    TextView txtItemPrice;

    public ItemsViewHolder(View itemView) {
        super(itemView);
        view = (CardView) itemView;
        imgItemImage = itemView.findViewById(R.id.imgItemImage);
        txtItemName = itemView.findViewById(R.id.txtItemName);
        txtItemDescription = itemView.findViewById(R.id.txtItemDescription);
        txtItemPrice = itemView.findViewById(R.id.txtItemPrice);

    }
}
