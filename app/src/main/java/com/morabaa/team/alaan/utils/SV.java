package com.morabaa.team.alaan.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.morabaa.team.alaan.R;
import com.morabaa.team.alaan.fragments.MainFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by eagle on 1/16/2018.
 */

public class SV {

    public static String verificationId = "";
    public static String CURRENT_FRAGMENT = "";
    public static String phone_auth_id;
    public static String fontCiro = "fonts/CairoRegular.ttf";

    public static void openfrg(Context ctx, Fragment fragment, String fragTag) {

        if (!CURRENT_FRAGMENT.equals(fragTag)) {
            FragmentActivity activity = (FragmentActivity) ctx;
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right);
            fragmentTransaction.replace(R.id.mainFrame, fragment);
            fragmentTransaction.commit();
        }
    }

    public static Boolean onBack(Activity ctx) {
        if (CURRENT_FRAGMENT.equals("MainFragment")) {
            return true;
        } else if (CURRENT_FRAGMENT.equals("ItemFragment")) {
            openfrg(ctx, new MainFragment(), "");
        } else if (CURRENT_FRAGMENT.equals("SearchFragment")) {
            openfrg(ctx, new MainFragment(), "");
        }
        return false;
    }

    public static Typeface BoldFont(Context ctx) {
        Typeface bold = Typeface.createFromAsset(ctx.getAssets(), "fonts/CairoBold.ttf");
        return bold;
    }

    public static class ScreenDimensions {

        public static int getHeight(Activity activity) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            return displayMetrics.heightPixels;
        }

        public static int getWidth(Activity activity) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            return displayMetrics.widthPixels;
        }
    }

    public static class API {

        public static final String MAIN = "192.168.106:49865/api/";
        public static final String CATEGORY = MAIN + "category/";
        public static final String ITEM = MAIN + "item/"; // list of all items
        public static final String USER = MAIN + "user/";
        public static final String IMAGE = MAIN + "image/";
        public static final String RATE = MAIN + "rate/";
        public static final String COMMENT = MAIN + "comment/";


    }


}
