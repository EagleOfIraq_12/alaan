package com.morabaa.team.alaan.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by thulfkcar on 2/1/2018.
 */

public abstract class Download {
    Context context;
    JSONObject jsonParms;
    String Url;
    private int requestType;

    public Download(Context context, int requestType, JSONObject jsonParms, String url) {
        this.context = context;
        this.requestType = requestType;
        this.jsonParms = jsonParms;
        Url = url;
        start();
    }

    void start() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String requestBody = "";
        if (jsonParms != null) {
            requestBody= jsonParms.toString();
        }
        final String finalRequestBody = requestBody;
        StringRequest stringRequest =
                new StringRequest(requestType, Url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        onReceive(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //............

                        Log.e("VOLLEY", error.toString());
                    }

                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return finalRequestBody == null ? null : finalRequestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", finalRequestBody, "utf-8");
                            return null;
                        }
                    }

                    @Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String parsed;
                        try {
                            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        } catch (UnsupportedEncodingException e) {
                            parsed = new String(response.data);
                        }
                        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                    }
                };

        requestQueue.add(stringRequest);
    }

    public abstract void onReceive(String json);
}

