package com.morabaa.team.alaan.fragments;


import static com.morabaa.team.alaan.Activities.MainActivity.db;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;
import com.morabaa.team.alaan.R;
import com.morabaa.team.alaan.adapters.CategoriesRecyclerViewAdapter;
import com.morabaa.team.alaan.adapters.ItemsRecyclerViewAdapter;
import com.morabaa.team.alaan.model.Category;
import com.morabaa.team.alaan.model.Item;
import com.morabaa.team.alaan.utils.SV;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */

public class MainFragment extends Fragment {
      
      RecyclerView itemsRecyclerView;
      RecyclerView categoriesRecyclerView;
      SearchView txtSearch;
      
      int curr = 0;
      int prev = 0;
      
      public MainFragment() {
            // Required empty public constructor
      }
      
      void onScroll(final RecyclerView view) {
            view.getViewTreeObserver().addOnScrollChangedListener(
                  new ViewTreeObserver.OnScrollChangedListener() {
                        
                        @Override
                        public void onScrollChanged() {
                              //   Toast.makeText(getContext(), ""+scrollViewSub.getScrollY(), Toast.LENGTH_SHORT).show();
                              if (view.getChildAt(0).getBottom() ==
                                    (view.getHeight() + view.getScrollY())) {
//                            bottomProgressBar.setVisibility(View.VISIBLE);
                                    new CountDownTimer(5000, 1) {
                                          
                                          @Override
                                          public void onTick(long millisUntilFinished) {
                                                // do something after 1s
                                          }
                                          
                                          @Override
                                          public void onFinish() {
                                                System.out.println("now : bottom");
                                          }
                                          
                                    }.start();
                              }
                              
                              int now = view.computeVerticalScrollOffset();
                              System.out.println("now : " + now + "\t" +
                                    "prev : " + prev);
                              if (prev < now) {
                                    if (curr != 1) {
                                          categoriesRecyclerView.animate()
                                                .alpha(0.0f)
                                                .y(0.0f)
                                                .setListener(new AnimatorListener() {
                                                      @Override
                                                      public void onAnimationStart(Animator animator) {
            
                                                      }
      
                                                      @Override
                                                      public void onAnimationEnd(
                                                            Animator animator) {
//                                                            categoriesRecyclerView.setVisibility(View.GONE);
                                                            categoriesRecyclerView.setMinimumHeight(0);
      
                                                      }
      
                                                      @Override
                                                      public void onAnimationCancel(
                                                            Animator animator) {
            
                                                      }
      
                                                      @Override
                                                      public void onAnimationRepeat(
                                                            Animator animator) {
            
                                                      }
                                                });
                                    }
                                    curr = 1;
                                    
                              } else {
                                    if (curr != 2) {
                                          categoriesRecyclerView.animate()
                                                .alpha(1.0f)
                                                .y(80.0f)
                                                .setListener(new AnimatorListener() {
                                                      @Override
                                                      public void onAnimationStart(Animator animator) {
//                                                            categoriesRecyclerView.setVisibility(View.VISIBLE);
                                                            categoriesRecyclerView.setMinimumHeight(60);
                                                      }
                  
                                                      @Override
                                                      public void onAnimationEnd(
                                                            Animator animator) {
                                                      }
                  
                                                      @Override
                                                      public void onAnimationCancel(
                                                            Animator animator) {
                        
                                                      }
                  
                                                      @Override
                                                      public void onAnimationRepeat(
                                                            Animator animator) {
                        
                                                      }
                                                });
                                    }
                                    curr = 2;
                                    
                              }
                              prev = now;
                        }
                  });
      }
      
      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main, container, false);
      }
      
      @SuppressLint({"CommitTransaction", "ClickableViewAccessibility"})
      @Override
      public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            SV.CURRENT_FRAGMENT = "MainFragment";
            
            txtSearch = getActivity().findViewById(R.id.txtSearch);
            txtSearch.setVisibility(View.VISIBLE);
            
            itemsRecyclerView = view.findViewById(R.id.itemsRecyclerView);
            
            List<Item> items = db.itemDao().ITEMS();
            
            Toast.makeText(getContext(), "" + db.itemDao().COUNT(), Toast.LENGTH_SHORT).show();
            
            final ItemsRecyclerViewAdapter itemsAdapter = new ItemsRecyclerViewAdapter(getContext(),
                  items,
                  R.layout.item_layout,
                  getActivity().getSupportFragmentManager().beginTransaction(),
                  getActivity());
            
            itemsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
            itemsRecyclerView.setAdapter(itemsAdapter);
//            onScroll(itemsRecyclerView);
            categoriesRecyclerView = view.findViewById(R.id.categoriesRecyclerView);
            List<Category> categories = db.categoryDao().CATEGORIES();
            
            categoriesRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false)
            );
            final CategoriesRecyclerViewAdapter categoriesAdapter = new CategoriesRecyclerViewAdapter(
                  getContext(), categories,
                  getActivity().getSupportFragmentManager().beginTransaction(),
                  getActivity(),
                  itemsRecyclerView,
                  items);
            categoriesRecyclerView.setAdapter(categoriesAdapter);
            
            txtSearch.setOnSearchClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        SV.openfrg(getContext(), new SearchFragment(), "SearchFragment");
                  }
            });

//        txtSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                callSearch(query);
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                callSearch(newText);
//
//                return true;
//            }
//
//            public void callSearch(String query) {
//               SV.openfrg(getContext(),new SearchFragment(),"SearchFragment");
////                List<Item> newItems = new ArrayList<>();
////                List<Item> currentItems = new ArrayList<>();
////
////                if (categoriesAdapter.getItems() == null) {
////                    currentItems = items;
////                } else {
////                    currentItems = categoriesAdapter.getItems();
////                }
////                for (Item item : currentItems) {
////                    if (item.getName().contains(query)) {
////                        newItems.add(item);
////                    }
////                }
////
////
////                itemsAdapter.setItems(newItems);
////
////                itemsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
////                itemsRecyclerView.setAdapter(itemsAdapter);
//
//            }
//
//        });
      
      }
      
}
