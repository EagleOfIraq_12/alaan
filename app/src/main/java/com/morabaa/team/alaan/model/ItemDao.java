package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by thulfkcar on 1/31/2018.
 */

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertItem(Item... items);


    @Delete
    void deleteItem(Item... items);

    @Query("DELETE FROM Item")
    void deleteAll();


    @Query("SELECT * FROM Item")
    List<Item> ITEMS();

    @Query("SELECT COUNT(id) FROM Item")
    int COUNT();

    @Update
    void UpdateItem(Item... items);


}