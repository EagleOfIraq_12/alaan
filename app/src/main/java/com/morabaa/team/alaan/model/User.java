package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;

import com.google.gson.Gson;
import com.morabaa.team.alaan.utils.LocalData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by thulfkcar on 1/27/2018.
 */
@Entity
public class User {

    //region prop
    @PrimaryKey(autoGenerate = true)
    long id;
    String name;
    String key;
    //endregion

    @Override
    public String toString() {
        return "User:{" +
                "id:" + id +
                ", name:'" + name + '\'' +
                ", key:'" + key +
                '}';
    }

    public static User setUserLoginState(Context ctx, User user) {
        LocalData.add(ctx, "UserJson", user.toString());

        return user;
    }

    public static void clearUserToLogout(Context ctx) {


        LocalData.add(ctx, "UserJson", new User().toString());

    }

    public static User getUserLoginState(Context ctx) {

        String serJsonString = LocalData.get(ctx, "UserJson");
        JSONObject object = null;
        try {
            object = new JSONObject(serJsonString.substring(serJsonString.indexOf("{"), serJsonString.lastIndexOf("}") + 1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        User user = gson.fromJson(String.valueOf(object), User.class);
        if (!user.getName().trim().equals("") && user.getId() != 0 && user.getKey().trim().length() > 0)
            return user;
        else return null;
    }

    //region getter and setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    //endregion
}
