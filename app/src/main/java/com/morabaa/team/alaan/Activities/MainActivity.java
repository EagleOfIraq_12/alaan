package com.morabaa.team.alaan.Activities;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.morabaa.team.alaan.R;
import com.morabaa.team.alaan.fragments.MainFragment;
import com.morabaa.team.alaan.model.Category;
import com.morabaa.team.alaan.model.DB;
import com.morabaa.team.alaan.model.Item;
import com.morabaa.team.alaan.model.User;
import com.morabaa.team.alaan.utils.Download;
import com.morabaa.team.alaan.utils.SV;

public class MainActivity extends AppCompatActivity {

    public static DB db;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = Room.databaseBuilder(getApplicationContext(),
                DB.class, "alaan").allowMainThreadQueries().build();

//        db.userDao().insertUser(
//                new User(){{
//                    setName("mmmm");
//                }}
//        );

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (db.itemDao().COUNT() == 0) {
            for (int i = 0; i < 150; i++) {
                final int finalI = i;
                db.itemDao().insertItem(
                        new Item() {{
                            setName("منتج " + finalI);
                            setDescription("alaa balaa بلا بلا بلااااا");
                            setPrice(finalI * 12 + finalI % 3);
                            setCategory("cat " + finalI);
                            setImageUrl("https://www.jawdatsaid.net/images/8/86/Clock_derp.png");
                        }}
                );
                db.categoryDao().insertCategory(
                        new Category() {{
                            setImageUrl("-");
                            setName("cat " + finalI);
                            setImageUrl("https://png.icons8.com/cotton/2x/cottage.png");
                        }}
                );
            }
        }
        System.out.println(SV.API.ITEM + "/GetItems");
        new Download(
                getApplicationContext(),
                Request.Method.GET,
                null,
                "http://httpbin.org/get"
//                SV.API.ITEM + "/GetItems"
        ) {
            @Override
            public void onReceive(String json) {
//                db.itemDao().deleteAll();
                for (Item item : Item.parse(json)) {
                    db.itemDao().insertItem(
                            item
                    );
                    Toast.makeText(MainActivity.this, ""+db.itemDao().COUNT(), Toast.LENGTH_SHORT).show();
                }
            }
        };
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        Fragment mainFragment = new MainFragment();
        fragmentTransaction.replace(R.id.mainFrame, mainFragment)
                .commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (SV.onBack(this)) {
                super.onBackPressed();
            }

        }
    }

    public void logOut(View view) {
        User.setUserLoginState(getApplication(), new User());
        Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
        this.startActivity(intent);
    }
}
