package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by thulfkcar on 1/31/2018.
 */

@Dao
public interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCategory(Category... categories);


    @Delete
    public void deleteCategory(Category... categories);


    @Query("SELECT * FROM Category")
    public List<Category> CATEGORIES();

    @Update
    public void UpdateCategory(Category... categories);


}