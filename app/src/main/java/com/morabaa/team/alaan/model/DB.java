package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by thulfkcar on 1/26/2018.
 */
@Database(entities = {
        User.class,
        Item.class,
        Comment.class,
        Category.class,
        ItemImage.class
}, version = 1)
public abstract class DB extends RoomDatabase {


    public abstract UserDao userDao();

    public abstract ItemDao itemDao();

    public abstract CategoryDao categoryDao();

    public abstract CommentDao commentDao();

    public abstract ItemImageDao itemImageDao();
}
