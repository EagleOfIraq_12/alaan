package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by thulfkcar on 2/1/2018.
 */

@Entity
public class ItemImage {
    @PrimaryKey(autoGenerate = true)
    int id;
    int itemId;
    String Url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
