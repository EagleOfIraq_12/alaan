package com.morabaa.team.alaan.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.morabaa.team.alaan.R;
import com.morabaa.team.alaan.model.Category;
import com.morabaa.team.alaan.model.Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eagle on 1/22/2018.
 */

public class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesViewHolder> {

    private FragmentTransaction fragmentTransaction;
    private List<Category> categories;
    private Context context;
    private Activity activity;
    private RecyclerView itemsRecyclerView;
    private List<Item> items;

    public CategoriesRecyclerViewAdapter(Context context, List<Category> categories,
                                         FragmentTransaction fragmentTransaction,
                                         Activity activity,
                                         RecyclerView itemsRecyclerView,
                                         List<Item> items
    ) {
        this.categories = categories;
        this.context = context;
        this.fragmentTransaction = fragmentTransaction;
        this.activity = activity;
        this.itemsRecyclerView = itemsRecyclerView;
        this.items = items;
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.category_layout, parent, false);
        return new CategoriesViewHolder(view);
    }

    List<Item> myItems;

    @Override
    public void onBindViewHolder(final CategoriesViewHolder holder, final int position) {
//        new DownloadImage() {
//            @Override
//            public void onDownload(Bitmap result) {
//                holder.imgCategoryImage.setImageBitmap(result);
//            }
//        }.execute(categories.get(position).getImageUrl());

        Picasso.with(context)
                .load(categories.get(position).getImageUrl())
                .into(holder.imgCategoryImage);

        holder.txtCategoryName.setText(
                categories.get(position).getName()
        );

        holder.view.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        myItems = new ArrayList<>();
                        for (Item item : items) {
                            if (item.getCategory().equals(categories.get(position).getName())) {
                                myItems.add(item);
                            }
                        }
                        itemsRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                        itemsRecyclerView.setAdapter(
                                new ItemsRecyclerViewAdapter(context, myItems,
                                        R.layout.item_layout, fragmentTransaction,
                                        activity)
                        );


                    }
                }
        );
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public List<Item> getItems() {
        return myItems;
    }

    public void setItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}

class CategoriesViewHolder extends RecyclerView.ViewHolder {

    LinearLayout view;
    ImageView imgCategoryImage;
    TextView txtCategoryName;

    public CategoriesViewHolder(View itemView) {
        super(itemView);
        view = (LinearLayout) itemView;
        imgCategoryImage = itemView.findViewById(R.id.imgCategoryImage);
        txtCategoryName = itemView.findViewById(R.id.txtCategoryName);

    }
}
