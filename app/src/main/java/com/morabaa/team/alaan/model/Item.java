package com.morabaa.team.alaan.model;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.morabaa.team.alaan.utils.SV;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by eagle on 1/22/2018.
 */
@Entity
public class Item {
    //region prop
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String name;
    private String description;
    private float price;
    private String category;
    private boolean available;
    private long categoryId;
    private String imageUrl;

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    private float rate;
    //endregion

    public Item() {
    }

    public static List<Item> parse(String s) {
        System.out.println("parse: "+s);

        List<Item> result = new ArrayList<>();
        try {
            JSONArray items = new JSONArray(s);
            JSONObject item = new JSONObject();
            for (int i = 0; i < items.length(); i++) {
                item = items.getJSONObject(i);
                final JSONObject finalItem = item;
                result.add(new Item() {{
                               setId(finalItem.getInt("Id"));
                               setName(finalItem.getString("Name"));
                               setDescription(finalItem.getString("Description"));
                               setRate((float) finalItem.getDouble("Rating"));
                               setPrice(Float.parseFloat(finalItem.getString("SalePrice")));
                               //image.....
                               setCategoryId(finalItem.getInt("CategoryId"));
                               setAvailable(finalItem.getBoolean("available"));

                           }}
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    //region g and s
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<Item> sort(List<Item> items, enumItemProperty enumItemProperty) {

        switch (enumItemProperty) {
            case name: {
                Collections.sort(items, new Comparator<Item>() {
                    @Override
                    public int compare(Item p1, Item p2) {
                        return p1.name
                                .compareTo(p2.name);
                    }
                });
                return items;
            }
            case price: {
                Collections.sort(items, new Comparator<Item>() {
                    @SuppressLint("NewApi")
                    @Override
                    public int compare(Item p1, Item p2) {
                        return Integer.compare(
                                ((int) p1.getPrice() * 100),
                                ((int) p2.getPrice() * 100)
                        );
                    }
                });
                return items;
            }
            default: {
                return items;
            }
        }
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public enum enumItemProperty {
        name,
        price
    }
    //endregion

}
