package com.morabaa.team.alaan.adapters;

/**
 * Created by eagle on 1/22/2018.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.morabaa.team.alaan.R;


public class ItemImagesPagerAdapter extends PagerAdapter {
      
      private int[] mResources = {
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_foreground,
            R.drawable.ic_menu_camera,
            R.drawable.ic_menu_gallery,
            R.drawable.ic_menu_send,
            R.drawable.ic_menu_share
      };
      Context mContext;
      LayoutInflater mLayoutInflater;
      
      public ItemImagesPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext
                  .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            
      }
      
      @Override
      public int getCount() {
            return mResources.length;
      }
      
      @Override
      public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageView) object);
      }
      
      @Override
      public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(mContext);
            imageView.setImageResource(mResources[position]);
            container.addView(imageView);
            
            return imageView;
      }
      
      @Override
      public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
      }
}