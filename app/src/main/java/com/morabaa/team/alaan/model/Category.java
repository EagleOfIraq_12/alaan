package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by eagle on 1/24/2018.
 */
@Entity
public class Category {
    //region prop
    @PrimaryKey(autoGenerate = true)
    long id;
    String name;
    String imageUrl;
    //endregion

    public Category() {
    }


    //region geters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    //endregion


}
