package com.morabaa.team.alaan.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by thulfkcar on 1/31/2018.
 */

@Dao
public interface CommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertComment(Comment... comments);


    @Delete
    public void deleteComment(Comment... comments);


    @Query("SELECT * FROM Comment")
    public List<Comment> CATEGORIES();

    @Update
    public void UpdateComment(Comment... comments);


}